This script takes a tagpro.eu ID and throws out some detailed statistics in csv format.

It uses the tagpro.eu API, found at https://tagpro.eu/?science

The Tagpro.eu match .json file is saved to data/ so that we aren't spamming the server.

Put id of the matches in id.txt separated with a comma and run index.php

Fetching of the data may be blocked by the imunify360-webshield used by tagpro.eu web server in that case the script won't work.
